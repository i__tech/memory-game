(() => {
    //vars
    let TILES_COUNT = 20;
    let tiles = []; 
    let clickedTiles = []; 
    let canGet = true; 
    let movesCount = 0; 
    let tilesPair = 0; 
    let loadingSection = $('.loading');
    let $gameBoard = $('#gameBoard').empty();
    const engineSrc = 'miniengine.php'; 

    bindEvents = () => {    
        const startGameButton = $('#startGame');
        const showHighScoreBtn = $('#showHighscore');
        const closeHighScoreButton = $('.close-highscore');

        startGameButton.on('click', (e) => {
            e.preventDefault();
            showPlayerName();                
        });
        showHighScoreBtn.on('click', (e) => {
            e.preventDefault();
            showHighScore();
        })
        closeHighScoreButton.on('click', (e) => {
            e.preventDefault();
            showStage('stageStart');
        })
    }  
    startGame = () => {
        tiles = [];
        clickedTiles = [];
        canGet = true;
        movesCount = 0;
        tilesPair = 0;           

        for (let i=0; i<TILES_COUNT; i++) {
            tiles.push(Math.floor(i/2));
        }

        for (let i=TILES_COUNT-1; i>0; i--) {
            let swap = Math.floor(Math.random()*i);
            let tmp = tiles[i];
            tiles[i] = tiles[swap];
            tiles[swap] = tmp;
        }

        for ( let i=0; i<TILES_COUNT; i++) {            
            let $cell = $('<div class="cell"></div>');
            let $tile = $('<div class="tile"><span class="avers"></span><span class="revers"></span></div>');
            $tile.addClass('card-type-'+tiles[i]); 
            $tile.data('cardType', tiles[i])
            $tile.data('index', i);

            $cell.append($tile);
            $gameBoard.append($cell);                               
        }
        $gameBoard.find('.cell .tile').on('click', function() {
            tileClicked($(this))
        });     
    }
    showMoves = (moves) => {
        let gamerMoves = $('#gameMoves');
        gamerMoves.html(moves);
    }
    tileClicked = (element) => {
        if (canGet) {            
            if (!clickedTiles.length || (element.data('index') != clickedTiles[0].data('index'))) {                
                clickedTiles.push(element);
                element.addClass('show');                
            }          
            if (clickedTiles.length >= 2) {
                canGet = false;
                if (clickedTiles[0].data('cardType') === clickedTiles[1].data('cardType')) {
                    setTimeout(function() {deleteTiles()}, 500);
                } else {
                    setTimeout(function() {resetTiles()}, 500);
                }
                movesCount++;
                showMoves(movesCount);
            }
        }
    }
    deleteTiles = () => {     
        clickedTiles[0].fadeOut(function() {
            $(this).remove();
        });
        clickedTiles[1].fadeOut(function() {
            $(this).remove();                        
        });
        tilesPair++;
        clickedTiles = new Array();
        canGet = true;
        if (tilesPair >= TILES_COUNT / 2) {
            gameOver();
        }        
    }
    resetTiles = () => {
        clickedTiles[0].removeClass('show');
        clickedTiles[1].removeClass('show');
        clickedTiles = new Array();
        canGet = true;
    }
    gameOver = () => { saveHighScore(); }
    showLoading = () => { loadingSection.show(); }
    hideLoading = () => { loadingSection.hide(); }
    
    showPlayerName = () => {
        showStage('stagePlayerName');
        const checkName = $('#checkName');
        let playerName = $('#playerName');
        let playerNameBox = $('.player-name-box');

        checkName.on('click', () => {
            if (playerName.val()!='') {
                playerNameBox.removeClass('error');
                startGame();
                showStage('stageGame');
            } else {
                playerNameBox.addClass('error');
                return false;
            }
        })                        
    }
    saveHighScore = function() {        
        showLoading();
        let playerName = $('#playerName').val();        
        $.ajax({
            url : engineSrc,
            type : 'POST',            
            data : {
                action : 'save',
                player : playerName,
                moves : movesCount
            },
                success : () => {
            },         
            error : () => {
                console.log('An error appear :( ')            
            },
            complete : function() {
                showHighScore();
                hideLoading();
            }
        })
    }
    showHighScore = function() {
        showLoading();
        $.ajax({
            url : engineSrc,
            type : 'POST',            
            data : {
                action : 'read'                
            },
            dataType : 'json',
            success : function(r) {                
                $('#highscoreBoard').empty();
                for ( let x=0; x<r.length; x++) {
                    let record = r[x];                    
                    let $div = $('<div class="line"><strong class="player">'+record.player+' :</strong><span class="moves">'+record.moves+'</span></div>');
                    $('#highscoreBoard').append($div);
                }                   
            },         
            error : () => {
                console.log('An error appear :( ')                
            },
            complete : () => {
                hideLoading();
                showStage('stageHighscore');
            }
        })   
    }
    showStage = (stage) => {
        const slideVar = $('[class^=slide-]');
        const stageVar =  $('#'+stage);

        slideVar.removeClass('show');
        stageVar.addClass('show');
    }    
    init = () => { bindEvents(); }
    init();
})();    